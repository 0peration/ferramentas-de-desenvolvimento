# Ferramentas de desenvolvimento

## Como instalar Eclipse IDE no Debian e Ubuntu

Eclipse é um IDE de ambiente de desenvolvimento integrado gratuito que é usado por programadores para escrever software principalmente em Java, mas também em outras linguagens de programação principais por meio de plug-ins Eclipse.

A versão mais recente do Eclipse IDE 2020‑06 não vem com pacotes binários pré-compilados específicos para distribuições Linux baseadas em Debian. Em vez disso, você pode instalar o Eclipse IDE em distribuições Linux baseadas no Ubuntu ou Debian por meio do arquivo de instalação compactado.

Neste tutorial, aprenderemos como instalar a última edição do Eclipse IDE 2020-06 no Ubuntu ou em distribuições Linux baseadas em Debian.

    A Desktop machine with a minimum of 2GB of RAM.
    Java 9 or higher installed in Debian based distributions.

Instale Eclipse IDE no Debian e Ubuntu

Um Java 9 ou JRE/JDK mais recente é necessário para instalar o Eclipse IDE e a maneira mais fácil de instalar o Oracle Java JDK usando PPA de terceiros, conforme mostrado.

$ sudo apt install default-jre

Para instalar o Eclipse IDE em seu sistema, primeiro, abra um navegador e vá para a página de download oficial do Eclipse e baixe a versão mais recente do pacote tar específico para sua arquitetura de distribuição Linux instalada.

Como alternativa, você também pode baixar o arquivo do instalador tarball do Eclipse IDE em seu sistema por meio do utilitário wget, emitindo o comando abaixo.

$ wget http://ftp.yz.yamagata-u.ac.jp/pub/eclipse/oomph/epp/2020-06/R/eclipse-inst-linux64.tar.gz

Depois que o download for concluído, navegue até o diretório onde o pacote de archive foi baixado, geralmente os diretórios de downloads de sua casa, e emita os comandos abaixo para iniciar a instalação do Eclipse IDE.

$ tar -xvf eclipse-inst-linux64.tar.gz 
$ cd eclipse-installer/
$ sudo ./eclipse-inst

O novo instalador do Eclipse lista os IDE disponíveis para usuários do Eclipse. Você pode escolher e clicar no pacote IDE que deseja instalar.

Em seguida, escolha a pasta onde deseja que o Eclipse seja instalado.

Assim que a instalação for concluída, você pode iniciar o Eclipse.
Instale Eclipse IDE via Snap no Ubuntu

Snap é uma implantação de software e sistema de gerenciamento de pacotes para gerenciar pacotes na distribuição Linux, você pode usar o snap para instalar o Eclipse IDE no Ubuntu 18.04 ou mais recente usando os comandos a seguir.

$ sudo apt install snapd
$ sudo snap install --classic eclipse

Depois de instalar o Eclipse, navegue até Visão Geral de Atividades, procure Eclipse e inicie-o ...

Isso é tudo! A versão mais recente do Eclipse IDE agora está instalada em seu sistema. Desfrute de programação com Eclipse IDE.
